import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import { Route } from 'react-router-dom';
import './index.scss';
import CardsPage from './containers/CharacterCards/CardsPage/CardsPage';
import CardPage from './containers/CharacterCards/CardPage/CardPage';
import LocationCardPage from './containers/LocationCards/LocationCardPage/LocationCardPage';


ReactDOM.render((
    <BrowserRouter>
        <App>
            <Route exact path="/" component={CardsPage} />
            <Route exact path="/character/" component={CardsPage} />
            <Route path="/character/:id" component={CardPage} />
            <Route path="/location/:id" component={LocationCardPage} />
        </App>
    </BrowserRouter>), 
    document.getElementById('root')
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
