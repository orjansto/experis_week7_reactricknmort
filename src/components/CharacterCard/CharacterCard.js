import React from 'react';
//import { directive } from '@babel/types';
import { NavLink } from 'react-router-dom';
import './CharacterCard.scss';

const CharacterCard = (props) => {

    var linkButton = null;
    if(props.showLink){
        linkButton = <NavLink to={{ pathname: '/character/' + props.character.id }} className="cardNameLink">{props.character.name}</NavLink>;
    }else{
        let uriLoc = props.character.location.url;
        var newUriLoc = uriLoc.substring(uriLoc.lastIndexOf('/location/'), uriLoc.length);
        let uriOri = props.character.origin.url;
        var newUriOri = uriOri.substring(uriOri.lastIndexOf('/location/'), uriOri.length);
        linkButton = (
            <React.Fragment>
                <h4 className="text">{props.character.name}</h4>
                <b>Species: {props.character.species}</b><br/>
                <b>Status: {props.character.status}</b><br/>
                <b>Gender: {props.character.gender}</b><br/>               
                <b>Location: 
                    <NavLink to={newUriLoc} className="cardInfoLink">{props.character.location.name}</NavLink><br/>
                </b>
                <b>Place of origin: 
                    <NavLink to={newUriOri} className="cardInfoLink">{props.character.origin.name}</NavLink><br/>
                </b>
            </React.Fragment>
            );
    }

    return (
        <React.Fragment>
            <div className={props.showLink ? "somethin" : "col-xs-0 col-sm-3 col-md-4 col-lg-4"}>
                
            </div>   
            <div className={props.showLink ? "col-xs-12 col-sm-6 col-md-4 col-lg-3" : "col-xs-12 col-sm-6 col-md-4 col-lg-4"}>
                <div className="card CharacterCard mt-2">
                    <div className="card-img">
                        <img src={props.character.image} alt={props.character.name} className="card-img-top" />

                        <div className={props.showLink ? "card-body cardBodyHidden" : "card-body"} >
                            {linkButton}
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
 
    )
}


export default CharacterCard;