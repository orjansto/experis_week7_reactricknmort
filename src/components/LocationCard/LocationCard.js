import React from 'react';
//import { directive } from '@babel/types';
import { NavLink } from 'react-router-dom';
import './LocationCard.scss';

const LocationCard = (props) => {

    var linkButton = null;
    if(props.showLink){
        linkButton = <NavLink to={{ pathname: '/location/' + props.location.id }} className="cardNameLink">{props.location.name}</NavLink>;
    }else{
        // let uriLoc = props.character.location.url;
        // var newUriLoc = uriLoc.substring(uriLoc.lastIndexOf('/location/'), uriLoc.length);
        // let uriOri = props.character.origin.url;
        // var newUriOri = uriOri.substring(uriOri.lastIndexOf('/location/'), uriOri.length);
        //     <b>Location: 
        //     <NavLink to={newUriLoc} className="cardInfoLink">{props.character.location.name}</NavLink><br/>
        // </b>
        // <b>Place of origin: 
        //     <NavLink to={newUriOri} className="cardInfoLink">{props.character.origin.name}</NavLink><br/>
        //</b>
        linkButton = (
            <React.Fragment>
                <h4 className="text">{props.location.name}</h4>
                <b>Type: {props.location.type}</b><br/>
                <b>Dimension: {props.location.dimension}</b><br/>            
            </React.Fragment>
            );
    }

    return (
        <React.Fragment>
            <div className={props.showLink ? "something" : "col-xs-0 col-sm-3 col-md-4 col-lg-4"}>
                
            </div>   
            <div className={props.showLink ? "col-xs-12 col-sm-6 col-md-4 col-lg-3" : "col-xs-12 col-sm-6 col-md-4 col-lg-4"}>
                <div className="card LocationCard mt-2">
                    <div className="card-img">
                        <div className={props.showLink ? "card-body cardBodyHidden" : "card-body"} >
                            {linkButton}
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
 
    )
}


export default LocationCard;