import React from 'react'

export default class SearchComponent extends React.Component {

    constructor(props) {
        super(props)
        
        this.handleSearchTextChange = this.handleSearchTextChange.bind(this)
    }

    handleSearchTextChange(e) {
        const input = e.target.value
        if(input){
            this.props.searchChar(input);
        }  
    }

    componentDidUpdate() {
        if (this.props.onChange) {
            this.props.onChange(this.state);
        }
    }

    render() {
        return (
            <>
                <div className="form-group">
                    <input onChange={this.handleSearchTextChange} placeholder="search" />
                </div>
            </>
        );
    }
}
