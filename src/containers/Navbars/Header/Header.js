import React from 'react';
import {NavLink} from 'react-router-dom';

import './Header.scss';

export default class Header extends React.Component{

    render(){
        return (
            <React.Fragment>
                <div className="headerTop">
                    <img src={"https://altvr.com/wp-content/uploads/2016/07/rick-and-morty-top-banner-two-1.png"} alt="" style={{margin: '0', width: '100%'}}></img>
                    <div className="navbarPlacement">
                        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                            <button className="navbar-toggler" 
                                type="button"
                                data-toggle="collapse" 
                                data-target="#navbarTogglerDemo03" 
                                aria-controls="navbarTogglerDemo03" 
                                aria-expanded="false" 
                                aria-label="Toggle navigation"
                            >
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="navbar-brand">
                                <img src="https://i.imgur.com/CMKHbzE.png" width="30" height="30" alt=""/>
                            </div>          
                            <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                                <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                                    <li className="nav-item active">
                                        <NavLink to="/" exact activeClassName="current">Home</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink to="/character/" exact activeClassName="current">Characters</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink to="/location/" exact activeClassName="current">Places</NavLink>
                                    </li>
                                </ul>              
                            </div>
                        </nav>
                    </div>
                </div>
            </React.Fragment>   
        );
    }
}