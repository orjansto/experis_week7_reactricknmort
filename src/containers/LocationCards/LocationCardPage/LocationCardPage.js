import React from 'react';
import '../../../components/LocationCard/LocationCard';
import LocationCard from '../../../components/LocationCard/LocationCard';

const apiURL = `https://rickandmortyapi.com/api/location/`;

export default class LocationCardPage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            location: null
        };

    }
    componentDidMount(){
        this.getLocationData();
    }
    getLocationData(){
        console.log("URL: " + apiURL +this.props.match.params.id); 
        let currentID = this.props.match.params.id;
            
        fetch(apiURL + currentID)
            .then(response => response.json())
            .then(data => {
                if(data){
                    console.log("this.props.match.params.id: "+this.state.charID);
                    this.setState({
                        location: data
                    });
                }else{
                    console.log('NO data.results from '+apiURL+this.state.charID);
                }
            });    
             
    }
    render(){
        const location = this.state.location;
        let locationCard = null;
        if(location){
            if(location.id){
                console.log("location.id: "+location.id);
                console.log("location.name: "+location.name)
                locationCard = (<LocationCard key={location.id} location={location} ></LocationCard>);
            }else{
                locationCard = (<p>No locationID present..</p>);
            }          
        }else{
            locationCard = (<p>No location present..</p>);
        }
        
        return (
            <React.Fragment>
                <div className="row">
                    {locationCard}
                </div>
            </React.Fragment>
        );
    }
}