import React from 'react';
import '../../../components/CharacterCard/CharacterCard';
import CharacterCard from '../../../components/CharacterCard/CharacterCard';
import SearchComponent from '../../Navbars/SearchComponent/SearchComponent'

const apiURL = `https://rickandmortyapi.com/api/character/`;

export default class CardsPage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            rickMorty: [],
            characterCards: [],
        };
        this.searchForChar = this.searchForChar.bind(this);
        
    }
    componentDidMount(){
        this.getData();
    }
    searchForChar(query){
        let search = query;
        console.log("Search query: "+query);
        this.getSearchData(search);
        
    }

    getSearchData(search){
        if(search){
            fetch('https://rickandmortyapi.com/api/character/?name='+search)
            .then(response => response.json())
            .then(data => {
                if(data.results){
                    this.setState({
                        rickMorty: data.results
                    });
                }else{
                    console.log('NO data.results from '+apiURL);
                }
            });
        }
    }
    getData(){    
        fetch('https://rickandmortyapi.com/api/character/')
            .then(response => response.json())
            .then(data => {
                if(data.results){
                    this.setState({
                        rickMorty: data.results
                    });
                }else{
                    console.log('NO data.results from '+apiURL);
                }
            });
    }
    render(){
        const characters = this.state.rickMorty;
        let RenderedCharacters;
        if(characters.length > 0){ //.length > 0
            RenderedCharacters = characters.map((character) => {
                return (<CharacterCard key={character.id} character={character} showLink={true}></CharacterCard>);
            }); 
        }else{
            RenderedCharacters = (        
                <div className={"col-xs-12 col-sm-12 col-md-12"}>
                    <div className="text-center">
                        <div className="spinner-border text-light" role="status">
                            <span className="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
            );
        }
        return (
            <React.Fragment>
                <h4 style={{color: '#B8F0AC'}}>Characters</h4>
                <SearchComponent searchChar={this.searchForChar}/>
                <br/>
                <div className="row">
                    {RenderedCharacters}
                </div>
            </React.Fragment>
        );
    }
}
