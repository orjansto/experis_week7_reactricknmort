import React from 'react';
import '../../../components/CharacterCard/CharacterCard';
import CharacterCard from '../../../components/CharacterCard/CharacterCard';

const apiURL = `https://rickandmortyapi.com/api/character/`;

export default class CardPage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            character: null
        };

    }
    componentDidMount(){
        this.getCharacterData();
    }
    getCharacterData(){
        console.log("URL: " + apiURL +this.props.match.params.id); 
        let currentID = this.props.match.params.id;
            
        fetch(apiURL + currentID)
            .then(response => response.json())
            .then(data => {
                if(data){
                    console.log("this.props.match.params.id: "+this.state.charID);
                    this.setState({
                        character: data
                    });
                }else{
                    console.log('NO data.results from '+apiURL+this.state.charID);
                }
            });    
             
    }
    render(){
        const character = this.state.character;
        let characterCard = null;
        if(character){
            if(character.id){
                console.log("character.id: "+character.id);
                console.log("character.name: "+character.name)
                characterCard = (<CharacterCard key={character.id} character={character} ></CharacterCard>);
            }else{
                characterCard = (<p>No characterID present..</p>);
            }          
        }else{
            characterCard = (<p>No character present..</p>);
        }
        
        return (
            <React.Fragment>
                <div className="row">
                    {characterCard}
                </div>
            </React.Fragment>
        );
    }
}

