import React from 'react';
//import logo from './logo.svg';
import './App.css';
import './containers/CharacterCards/CardsPage/CardsPage';
//import CardsPage from './containers/CharacterCards/CardsPage/CardsPage';
//import { tsPropertySignature } from '@babel/types';
import Header from './containers/Navbars/Header/Header';


export default class App extends React.Component{
  render(){
    return (
      <div className="App">
        <header className="App-header">
          <Header />
        </header>
        <div style={{backgroundColor: '#110E2D', minHeight: '100vh'}}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

